package org.abbtech.practice.service.impl;

import org.abbtech.practice.dto.*;
import org.abbtech.practice.model.CategoryIncome;
import org.abbtech.practice.model.CurrencyEnum;
import org.abbtech.practice.model.Income;
import org.abbtech.practice.repository.CategoryIncomeRepository;
import org.abbtech.practice.repository.IncomeRepository;
import org.abbtech.practice.service.client.ExchangeClient;
import org.abbtech.practice.service.client.UserClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class IncomeOperationsServiceImplTest {

    @Mock
    private IncomeRepository incomeRepository;

    @Mock
    private CategoryIncomeRepository categoryIncomeRepository;

    @Mock
    private UserClient userClient;

    @Mock
    private ExchangeClient exchangeClient;

    @InjectMocks
    private IncomeOperationsServiceImpl incomeService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void addIncomeRequest_ValidData_ReturnsSuccessMessage() {
        String userId = UUID.randomUUID().toString();
        IncomeDTO incomeDTO = new IncomeDTO("Salary", "Monthly salary", 5000.0, LocalDateTime.now(), CurrencyEnum.USD, "Salary");

        CategoryIncome categoryIncome = new CategoryIncome();
        categoryIncome.setCategoryName("Salary");

        ExchangeRateDTO exchangeRateDTO = new ExchangeRateDTO("USD", 1.7, 8500.0); // Mocked conversion rate and amount
        AccountResponse accountResponse = new AccountResponse(BigDecimal.valueOf(15000.0), BigDecimal.valueOf(20000.0));

        when(categoryIncomeRepository.findCategoryIncomeByCategoryName(anyString())).thenReturn(categoryIncome);
        when(incomeRepository.save(any(Income.class))).thenAnswer(invocation -> invocation.getArgument(0));
        when(exchangeClient.convertToAZN(any(ExchangeRateRequest.class))).thenReturn(exchangeRateDTO);
        when(userClient.getBalanceAndLimitByUserId(anyString())).thenReturn(new ResponseEntity<>(accountResponse, HttpStatus.OK));

        ResponseEntity<String> response = incomeService.addIncomeRequest(userId, incomeDTO);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(incomeRepository, times(1)).save(any(Income.class));
        verify(userClient, times(1)).updateBalance(anyString(), any(BalanceRequest.class));
    }

    @Test
    void addIncomeRequest_InvalidUUID_ReturnsBadRequest() {
        String invalidUserId = "invalid-uuid";
        IncomeDTO incomeDTO = new IncomeDTO("Salary", "Monthly salary", 5000.0, LocalDateTime.now(), CurrencyEnum.USD, "Salary");

        ResponseEntity<String> response = incomeService.addIncomeRequest(invalidUserId, incomeDTO);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        verify(incomeRepository, times(0)).save(any(Income.class));
    }

    @Test
    void addIncomeRequest_ExceptionThrown_ReturnsInternalServerError() {
        String userId = UUID.randomUUID().toString();
        IncomeDTO incomeDTO = new IncomeDTO("Salary", "Monthly salary", 5000.0, LocalDateTime.now(), CurrencyEnum.USD, "Salary");

        when(categoryIncomeRepository.findCategoryIncomeByCategoryName(anyString())).thenThrow(new RuntimeException("Unexpected error"));

        try {
            incomeService.addIncomeRequest(userId, incomeDTO);
        } catch (RuntimeException e) {
            assertEquals("An unexpected error occurred while adding the income.", e.getMessage());
        }
    }

    @Test
    void editIncomeRequest_ValidData_ReturnsSuccessMessage() {
        String userId = UUID.randomUUID().toString();
        Long incomeId = 1L;
        IncomeDTO incomeDTO = new IncomeDTO("Part-time", "Freelance work", 3000.0, LocalDateTime.now(), CurrencyEnum.EUR, "Part-time");

        Income income = new Income();
        income.setId(incomeId);
        income.setUserId(UUID.fromString(userId));
        CategoryIncome categoryIncome = new CategoryIncome();
        categoryIncome.setCategoryName("Part-time");

        ExchangeRateDTO exchangeRateDTO = new ExchangeRateDTO("USD", 1.7, 5100.0); // Mocked conversion rate and amount
        AccountResponse accountResponse = new AccountResponse(BigDecimal.valueOf(15000.0), BigDecimal.valueOf(20000.0));

        when(incomeRepository.findIncomeById(incomeId)).thenReturn(Optional.of(income));
        when(categoryIncomeRepository.findCategoryIncomeByCategoryName(anyString())).thenReturn(categoryIncome);
        when(incomeRepository.save(any(Income.class))).thenAnswer(invocation -> invocation.getArgument(0));
        when(exchangeClient.convertToAZN(any(ExchangeRateRequest.class))).thenReturn(exchangeRateDTO);
        when(userClient.getBalanceAndLimitByUserId(anyString())).thenReturn(new ResponseEntity<>(accountResponse, HttpStatus.OK));

        ResponseEntity<String> response = incomeService.editIncomeRequest(userId, incomeId, incomeDTO);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(incomeRepository, times(1)).save(any(Income.class));
    }

    @Test
    void deleteIncomeRequest_ValidData_ReturnsSuccessMessage() {
        String userId = UUID.randomUUID().toString();
        Long incomeId = 1L;
        Income income = new Income();
        income.setId(incomeId);
        income.setUserId(UUID.fromString(userId));
        income.setAmount(5000.0);

        ExchangeRateDTO exchangeRateDTO = new ExchangeRateDTO("USD", 1.7, 8500.0); // Mocked conversion rate and amount
        AccountResponse accountResponse = new AccountResponse(BigDecimal.valueOf(15000.0), BigDecimal.valueOf(20000.0));

        when(incomeRepository.findIncomeById(incomeId)).thenReturn(Optional.of(income));
        when(exchangeClient.convertToAZN(any(ExchangeRateRequest.class))).thenReturn(exchangeRateDTO);
        when(userClient.getBalanceAndLimitByUserId(anyString())).thenReturn(new ResponseEntity<>(accountResponse, HttpStatus.OK));

        ResponseEntity<String> response = incomeService.deleteIncomeRequest(userId, incomeId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(incomeRepository, times(1)).delete(any(Income.class));
    }

    @Test
    void getAllIncomesRequest_ValidData_ReturnsIncomeList() {
        String userId = UUID.randomUUID().toString();
        Income income = new Income();
        income.setId(1L);
        income.setTitle("Salary");
        income.setUserId(UUID.fromString(userId));

        CategoryIncome categoryIncome = new CategoryIncome();
        categoryIncome.setCategoryName("Salary");

        income.setCategoryIncome(categoryIncome);

        when(incomeRepository.findIncomesByUserId(any(UUID.class))).thenReturn(Optional.of(List.of(income)));

        ResponseEntity<List<IncomeDTO>> response = incomeService.getAllIncomesRequest(userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, Objects.requireNonNull(response.getBody()).size());
        assertEquals("Salary", response.getBody().getFirst().title());
    }

    @Test
    void getIncomesOfMonthRequest_ValidData_ReturnsIncomeList() {
        String userId = UUID.randomUUID().toString();
        int month = 7;
        Income income = new Income();
        income.setId(1L);
        income.setTitle("Part-time");
        income.setUserId(UUID.fromString(userId));

        CategoryIncome categoryIncome = new CategoryIncome();
        categoryIncome.setCategoryName("Part-time");

        income.setCategoryIncome(categoryIncome);

        when(incomeRepository.findIncomesByUserIdAndMonth(any(UUID.class), eq(month))).thenReturn(Optional.of(List.of(income)));

        ResponseEntity<List<IncomeDTO>> response = incomeService.getIncomesOfMonthRequest(userId, month);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, Objects.requireNonNull(response.getBody()).size());
        assertEquals("Part-time", response.getBody().getFirst().title());
    }

    @Test
    void getIncomesOfCategoryRequest_ValidData_ReturnsIncomeList() {
        String userId = UUID.randomUUID().toString();
        String categoryName = "Salary";
        Income income = new Income();
        income.setId(1L);
        income.setTitle("Salary");
        income.setUserId(UUID.fromString(userId));

        CategoryIncome categoryIncome = new CategoryIncome();
        categoryIncome.setCategoryName("Salary");

        income.setCategoryIncome(categoryIncome);

        when(incomeRepository.findIncomesByUserIdAndCategory(any(UUID.class), eq(categoryName))).thenReturn(Optional.of(List.of(income)));

        ResponseEntity<List<IncomeDTO>> response = incomeService.getIncomesOfCategoryRequest(userId, categoryName);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, Objects.requireNonNull(response.getBody()).size());
        assertEquals("Salary", response.getBody().getFirst().title());
    }

    @Test
    void getIncomesOfMonthAndCategoryRequest_ValidData_ReturnsIncomeList() {
        String userId = UUID.randomUUID().toString();
        int month = 7;
        String categoryName = "Salary";
        Income income = new Income();
        income.setId(1L);
        income.setTitle("Salary");
        income.setUserId(UUID.fromString(userId));

        CategoryIncome categoryIncome = new CategoryIncome();
        categoryIncome.setCategoryName("Salary");

        income.setCategoryIncome(categoryIncome);

        when(incomeRepository.findIncomesByUserIdAndMonthAndCategory(any(UUID.class), eq(month), eq(categoryName))).thenReturn(Optional.of(List.of(income)));

        ResponseEntity<List<IncomeDTO>> response = incomeService.getIncomesOfMonthAndCategoryRequest(userId, month, categoryName);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1, Objects.requireNonNull(response.getBody()).size());
        assertEquals("Salary", response.getBody().getFirst().title());
    }

    @Test
    void getTotalIncome_ValidData_ReturnsTotalIncome() {
        String userId = UUID.randomUUID().toString();
        Income income = new Income();
        income.setId(1L);
        income.setAmount(1000.0);
        income.setCurrencyName(CurrencyEnum.AZN);
        income.setUserId(UUID.fromString(userId));

        when(incomeRepository.findIncomesByUserId(any(UUID.class))).thenReturn(Optional.of(List.of(income)));

        ResponseEntity<Double> response = incomeService.getTotalIncome(userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1000.0, response.getBody());
    }

    @Test
    void getMonthlyTotalIncome_ValidData_ReturnsMonthlyTotalIncome() {
        String userId = UUID.randomUUID().toString();
        int month = 7;
        Income income = new Income();
        income.setId(1L);
        income.setAmount(1000.0);
        income.setCurrencyName(CurrencyEnum.AZN);
        income.setUserId(UUID.fromString(userId));

        when(incomeRepository.findIncomesByUserIdAndMonth(any(UUID.class), eq(month))).thenReturn(Optional.of(List.of(income)));

        ResponseEntity<Double> response = incomeService.getMonthlyTotalIncome(userId, month);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(1000.0, response.getBody());
    }
}