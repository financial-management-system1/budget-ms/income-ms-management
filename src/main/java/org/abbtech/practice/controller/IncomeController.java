package org.abbtech.practice.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.IncomeDTO;
import org.abbtech.practice.service.impl.IncomeOperationsServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@Validated
@RequiredArgsConstructor
public class IncomeController {
    private final IncomeOperationsServiceImpl incomeService;

    @PostMapping("/add")
    public ResponseEntity<String> addIncome(@RequestHeader("X-USER-ID") @NotBlank String userId, @RequestBody @Valid IncomeDTO incomeRequest) {
        return incomeService.addIncomeRequest(userId, incomeRequest);
    }

    @PutMapping("/edit")
    public ResponseEntity<String> editIncome(@RequestHeader("X-USER-ID") @NotBlank String userId, @RequestParam Long incomeId, @RequestBody @Valid IncomeDTO incomeRequest) {
        return incomeService.editIncomeRequest(userId, incomeId, incomeRequest);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteIncome(@RequestHeader("X-USER-ID") @NotBlank String userId, @RequestParam Long incomeId) {
        return incomeService.deleteIncomeRequest(userId, incomeId);
    }

    @GetMapping("/get")
    public ResponseEntity<List<IncomeDTO>> getAllIncomes(@RequestHeader("X-USER-ID") @NotBlank String userId) {
        return incomeService.getAllIncomesRequest(userId);
    }

    @GetMapping("/getByMonth")
    public ResponseEntity<List<IncomeDTO>> getIncomesOfMonth(@RequestHeader("X-USER-ID") @NotBlank String userId, @RequestParam @Valid @Min(1) @Max(12) int month) {
        return incomeService.getIncomesOfMonthRequest(userId, month);
    }

    @GetMapping("/getByCategory")
    public ResponseEntity<List<IncomeDTO>> getIncomesOfCategory(@RequestHeader("X-USER-ID") @NotBlank String userId, @RequestParam @Valid @NotBlank String categoryName) {
        return incomeService.getIncomesOfCategoryRequest(userId, categoryName);
    }

    @GetMapping("/getByMonthAndCategory")
    public ResponseEntity<List<IncomeDTO>> getIncomesOfMonthAndCategory(@RequestHeader("X-USER-ID") @NotBlank String userId, @RequestParam @Valid @Min(1) @Max(12) int month, @RequestParam @Valid @NotBlank String categoryName) {
        return incomeService.getIncomesOfMonthAndCategoryRequest(userId, month, categoryName);
    }

    @GetMapping("/getTotalIncome")
    public ResponseEntity<Double> getTotalIncomeRequest(@RequestHeader("X-USER-ID") @NotBlank String userId) {
        return incomeService.getTotalIncome(userId);
    }

    @GetMapping("/getMonthlyTotalIncome")
    public ResponseEntity<Double> getMonthlyTotalIncomeRequest(@RequestHeader("X-USER-ID") @NotBlank String userId, @RequestParam @Valid @Min(1) @Max(12) int month) {
        return incomeService.getMonthlyTotalIncome(userId, month);
    }
}