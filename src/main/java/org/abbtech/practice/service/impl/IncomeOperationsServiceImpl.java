package org.abbtech.practice.service.impl;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.dto.*;
import org.abbtech.practice.exception.ResourceNotFoundException;
import org.abbtech.practice.model.CategoryIncome;
import org.abbtech.practice.model.Income;
import org.abbtech.practice.repository.CategoryIncomeRepository;
import org.abbtech.practice.repository.IncomeRepository;
import org.abbtech.practice.service.IncomeOperationsService;
import org.abbtech.practice.service.client.ExchangeClient;
import org.abbtech.practice.service.client.UserClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service("incomeOperationsServiceImpl")
@RequiredArgsConstructor
@Validated
@Transactional
public class IncomeOperationsServiceImpl implements IncomeOperationsService {
    private final IncomeRepository incomeRepository;
    private final CategoryIncomeRepository categoryIncomeRepository;
    private final UserClient userClient;
    private final ExchangeClient exchangeClient;

    @Override
    public ResponseEntity<String> addIncomeRequest(String userIdRequest, IncomeDTO incomeDTO) {
        String message;
        try {
            UUID userId = UUID.fromString(userIdRequest);

            CategoryIncome categoryIncome = categoryIncomeRepository.findCategoryIncomeByCategoryName(incomeDTO.categoryName());
            if (categoryIncome == null) {
                categoryIncome = categoryIncomeRepository.findCategoryIncomeByCategoryName("Others"); // default category
            }

            // user can enter date or it will be adjusted automatically
            LocalDateTime lastModifiedDate = incomeDTO.lastModifiedDate() != null ? incomeDTO.lastModifiedDate() : LocalDateTime.now().withNano(0);

            Income income = Income.builder()
                    .title(incomeDTO.title())
                    .notes(incomeDTO.notes())
                    .amount(incomeDTO.amount())
                    .lastModifiedDate(lastModifiedDate)
                    .userId(userId)
                    .currencyName(incomeDTO.currencyName())
                    .categoryIncome(categoryIncome)
                    .build();

            double previousIncome = calculateTotalIncomeInAZN(userId);
            incomeRepository.save(income);
            double newIncome = calculateTotalIncomeInAZN(userId);

            double difference = newIncome - previousIncome;
            double newBalance = updateUserBalance(userId, difference);

            message = "Income was added successfully with properties: " + "\n" +
                    "- ID: " + income.getId() + "\n" +
                    "- Title: " + income.getTitle() + "\n" +
                    "- Notes: " + income.getNotes() + "\n" +
                    "- Amount: " + income.getAmount() + "\n" +
                    "- Created Date: " + income.getLastModifiedDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n" +
                    "- UserID: " + income.getUserId().toString() + "\n" +
                    "- Currency Name: " + income.getCurrencyName().name() + "\n" +
                    "- Category Name: " + income.getCategoryIncome().getCategoryName() + "\n" +
                    "- Updated Balance: " + newBalance;

            return ResponseEntity.ok(message);
        } catch (IllegalArgumentException e) {
            message = "Invalid UUID format for userId: " + userIdRequest;
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            message = "An unexpected error occurred while adding the income.";
            throw new RuntimeException(message, e);
        }
    }

    @Override
    public ResponseEntity<String> editIncomeRequest(String userIdRequest, Long incomeId, IncomeDTO incomeDTO) {
        String message;
        try {
            Income income = incomeRepository.findIncomeById(incomeId)
                    .orElseThrow(() -> new ResourceNotFoundException("Income not found with ID: " + incomeId));

            CategoryIncome categoryIncome = categoryIncomeRepository.findCategoryIncomeByCategoryName(incomeDTO.categoryName());
            if (categoryIncome == null) {
                categoryIncome = categoryIncomeRepository.findCategoryIncomeByCategoryName("Others");
            }

            // user can enter date or it will be adjusted automatically
            LocalDateTime lastModifiedDate = incomeDTO.lastModifiedDate() != null ? incomeDTO.lastModifiedDate() : LocalDateTime.now().withNano(0);

            income.setTitle(incomeDTO.title());
            income.setNotes(incomeDTO.notes());
            income.setAmount(incomeDTO.amount());
            income.setLastModifiedDate(lastModifiedDate);
            income.setUserId(UUID.fromString(userIdRequest));
            income.setCurrencyName(incomeDTO.currencyName());
            income.setCategoryIncome(categoryIncome);

            double previousIncome = calculateTotalIncomeInAZN(UUID.fromString(userIdRequest));
            incomeRepository.save(income);
            double newIncome = calculateTotalIncomeInAZN(UUID.fromString(userIdRequest));

            double difference = newIncome - previousIncome;
            double newBalance = updateUserBalance(UUID.fromString(userIdRequest), difference);

            message = "Income updated successfully. Current balance: " + newBalance;
            return ResponseEntity.ok(message);
        } catch (IllegalArgumentException e) {
            message = "Invalid UUID format: " + userIdRequest;
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
        } catch (ResourceNotFoundException e) {
            message = e.getMessage();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
        } catch (Exception e) {
            message = "An unexpected error occurred while editing the income.";
            throw new RuntimeException(message, e);
        }
    }

    @Override
    public ResponseEntity<String> deleteIncomeRequest(String userIdRequest, Long incomeId) {
        String message;
        try {
            Optional<Income> incomeOptional = incomeRepository.findIncomeById(incomeId);

            if (incomeOptional.isPresent()) {
                Income income = incomeOptional.get();

                double previousIncome = calculateTotalIncomeInAZN(UUID.fromString(userIdRequest));
                incomeRepository.delete(income);
                double newIncome = calculateTotalIncomeInAZN(UUID.fromString(userIdRequest));

                double difference = newIncome - previousIncome;
                double newBalance = updateUserBalance(UUID.fromString(userIdRequest), difference);

                message = "Income deleted successfully. New balance: " + newBalance;
                return ResponseEntity.ok(message);
            } else {
                message = "Income not found with ID: " + incomeId;
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(message);
            }
        } catch (IllegalArgumentException e) {
            message = "Invalid UUID format for userId";
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
        } catch (Exception e) {
            message = "An unexpected error occurred while deleting the income.";
            throw new RuntimeException(message, e);
        }
    }

    @Override
    public ResponseEntity<List<IncomeDTO>> getAllIncomesRequest(String userIdRequest) {
        try {
            UUID userId = UUID.fromString(userIdRequest);
            List<Income> incomes = incomeRepository.findIncomesByUserId(userId).orElse(Collections.emptyList());

            if (incomes.isEmpty()) {
                return ResponseEntity.noContent().build();
            }

            List<IncomeDTO> incomeDTOS = new LinkedList<>();
            for (Income i : incomes) {
                incomeDTOS.add(convertToIncomeDTO(i));
            }

            return ResponseEntity.ok(incomeDTOS);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid UUID format: " + userIdRequest);
        } catch (Exception e) {
            throw new RuntimeException("An unexpected error occurred while fetching incomes.", e);
        }
    }

    @Override
    public ResponseEntity<List<IncomeDTO>> getIncomesOfMonthRequest(String userIdRequest, int month) {
        try {
            UUID userId = UUID.fromString(userIdRequest);

            Optional<List<Income>> incomes = incomeRepository.findIncomesByUserIdAndMonth(userId, month);

            if (incomes.isPresent() && !incomes.get().isEmpty()) {
                return ResponseEntity.ok(convertToIncomeDTOs(incomes.get()));
            } else {
                return ResponseEntity.noContent().build();
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid UUID format: " + userIdRequest);
        } catch (Exception e) {
            throw new RuntimeException("An unexpected error occurred while fetching incomes by month.", e);
        }
    }

    @Override
    public ResponseEntity<List<IncomeDTO>> getIncomesOfCategoryRequest(String userIdRequest, String categoryName) {
        try {
            UUID userId = UUID.fromString(userIdRequest);

            Optional<List<Income>> incomes = incomeRepository.findIncomesByUserIdAndCategory(userId, categoryName);

            if (incomes.isPresent() && !incomes.get().isEmpty()) {
                return ResponseEntity.ok(convertToIncomeDTOs(incomes.get()));
            } else {
                return ResponseEntity.noContent().build();
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid UUID format: " + userIdRequest);
        } catch (Exception e) {
            throw new RuntimeException("An unexpected error occurred while fetching incomes by category.", e);
        }
    }

    @Override
    public ResponseEntity<List<IncomeDTO>> getIncomesOfMonthAndCategoryRequest(String userIdRequest, int month, String categoryName) {
        try {
            UUID userId = UUID.fromString(userIdRequest);

            Optional<List<Income>> incomes = incomeRepository.findIncomesByUserIdAndMonthAndCategory(userId, month, categoryName);

            if (incomes.isPresent() && !incomes.get().isEmpty()) {
                return ResponseEntity.ok(convertToIncomeDTOs(incomes.get()));
            } else {
                return ResponseEntity.noContent().build();
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid UUID format: " + userIdRequest);
        } catch (Exception e) {
            throw new RuntimeException("An unexpected error occurred while fetching incomes by month and category.", e);
        }
    }

    private IncomeDTO convertToIncomeDTO(Income income) {
        return new IncomeDTO(
                income.getTitle(),
                income.getNotes(),
                income.getAmount(),
                income.getLastModifiedDate(),
                income.getCurrencyName(),
                income.getCategoryIncome().getCategoryName()
        );
    }

    private List<IncomeDTO> convertToIncomeDTOs(List<Income> incomes) {
        List<IncomeDTO> incomeDTOS = new LinkedList<>();

        for (Income i : incomes) {
            incomeDTOS.add(convertToIncomeDTO(i));
        }

        return incomeDTOS;
    }

    @Override
    public ResponseEntity<Double> getTotalIncome(String userIdRequest) {
        try {
            double totalIncome = calculateTotalIncomeInAZN(UUID.fromString(userIdRequest));
            return ResponseEntity.ok(totalIncome);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid UUID format: " + userIdRequest);
        }
    }

    @Override
    public ResponseEntity<Double> getMonthlyTotalIncome(String userIdRequest, int month) {
        try {
            double monthlyTotalIncome = calculateMonthlyTotalIncomeInAZN(UUID.fromString(userIdRequest), month);
            return ResponseEntity.ok(monthlyTotalIncome);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid UUID format: " + userIdRequest);
        }
    }

    private double calculateTotalIncomeInAZN(UUID userId) {
        List<Income> incomes = incomeRepository.findIncomesByUserId(userId).orElse(Collections.emptyList());
        return incomes.stream().mapToDouble(this::convertToAZN).sum();
    }

    private double calculateMonthlyTotalIncomeInAZN(UUID userId, int month) {
        List<Income> incomes = incomeRepository.findIncomesByUserIdAndMonth(userId, month).orElse(Collections.emptyList());
        return incomes.stream().mapToDouble(this::convertToAZN).sum();
    }

    private double convertToAZN(Income income) {
        if ("AZN".equals(income.getCurrencyName().name())) {
            return income.getAmount();
        }
        ExchangeRateDTO responseEntity = exchangeClient.convertToAZN(new ExchangeRateRequest(income.getCurrencyName().name(), income.getAmount(), null));
        if (Objects.nonNull(responseEntity.convertedAmount())) {
            return responseEntity.convertedAmount();
        }
        throw new RuntimeException("Failed to convert currency to AZN.");
    }

    private Double updateUserBalance(UUID userId, double totalIncome) {
        ResponseEntity<AccountResponse> accountResponse = userClient.getBalanceAndLimitByUserId(userId.toString());
        double newBalance = Objects.requireNonNull(accountResponse.getBody()).balance().doubleValue() + totalIncome;
        BalanceRequest updateBalanceRequest = new BalanceRequest();
        updateBalanceRequest.setBalance(BigDecimal.valueOf(newBalance));
        userClient.updateBalance(userId.toString(), updateBalanceRequest);

        return newBalance;
    }
}