package org.abbtech.practice.service;

import jakarta.validation.Valid;
import org.abbtech.practice.dto.IncomeDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface IncomeOperationsService {
    ResponseEntity<String> addIncomeRequest(String userId, @Valid IncomeDTO userRequest);
    ResponseEntity<String> editIncomeRequest(String userId, Long incomeId, @Valid IncomeDTO userRequest);
    ResponseEntity<String> deleteIncomeRequest(String userId, Long incomeId);
    ResponseEntity<List<IncomeDTO>> getAllIncomesRequest(String userId);
    ResponseEntity<List<IncomeDTO>> getIncomesOfMonthRequest(String userId, int month);
    ResponseEntity<List<IncomeDTO>> getIncomesOfCategoryRequest(String userId, String categoryName);
    ResponseEntity<List<IncomeDTO>> getIncomesOfMonthAndCategoryRequest(String userId, int month, String categoryName);
    ResponseEntity<Double> getTotalIncome(String userId);
    ResponseEntity<Double> getMonthlyTotalIncome(String userId, int month);
}