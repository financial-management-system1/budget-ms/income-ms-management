package org.abbtech.practice.service.client;

import org.abbtech.practice.dto.AccountResponse;
import org.abbtech.practice.dto.BalanceRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

@FeignClient(name = "user-service", url = "http://localhost:8081/api/user/account")
public interface UserClient {
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<AccountResponse> getBalanceAndLimitByUserId(@RequestHeader("X-USER-ID") String userId);

    @PutMapping
    ResponseEntity<String> updateBalance(@RequestHeader("X-USER-ID") String userId, @RequestBody BalanceRequest balanceRequest);
}