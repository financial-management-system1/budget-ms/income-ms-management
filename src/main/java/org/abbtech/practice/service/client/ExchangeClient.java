package org.abbtech.practice.service.client;

import org.abbtech.practice.dto.ExchangeRateDTO;
import org.abbtech.practice.dto.ExchangeRateRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "exchange-service", url = "http://localhost:8089/exchange")
public interface ExchangeClient {

    @PostMapping("/converted")
    ExchangeRateDTO convertToAZN(ExchangeRateRequest request);
}