package org.abbtech.practice.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;

@JsonDeserialize(using = CurrencyEnumDeserializer.class)
@Getter
public enum CurrencyEnum {
    AZN("1 Azərbaycan manatı"),
    USD("1 ABŞ dolları"),
    EUR("1 Avro"),
    AUD("1 Avstraliya dolları"),
    ARS("1 Argentina pesosu"),
    BYN("1 Belarus rublu"),
    BRL("1 Braziliya realı"),
    AED("1 BƏƏ dirhəmi"),
    ZAR("1 Cənubi Afrika randı"),
    KRW("100 Cənubi Koreya vonu"),
    CZK("1 Çexiya kronu"),
    CLP("100 Çili pesosu"),
    CNY("1 Çin yuanı"),
    DKK("1 Danimarka kronu"),
    GEL("1 Gürcüstan larisi"),
    HKD("1 Honq Konq dolları"),
    INR("1 Hindistan rupisi"),
    GBP("1 İngiltərə funt sterlinqi"),
    IDR("100 İndoneziya rupiası"),
    IRR("100 İran rialı"),
    SEK("1 İsveç kronu"),
    CHF("1 İsveçrə frankı"),
    ILS("1 İsrail şekeli"),
    CAD("1 Kanada dolları"),
    KWD("1 Küveyt dinarı"),
    KZT("1 Qazaxıstan tengəsi"),
    KGS("1 Qırğızıstan somu"),
    LBP("100 Livan funtu"),
    MYR("1 Malayziya rinqqiti"),
    MXN("1 Meksika pesosu"),
    MDL("1 Moldova leyi"),
    EGP("1 Misir funtu"),
    NOK("1 Norveç kronu"),
    UZS("100 Özbəkistan somu"),
    PLN("1 Polşa zlotısı"),
    RUB("1 Rusiya rublu"),
    SGD("1 Sinqapur dolları"),
    SAR("1 Səudiyyə Ərəbistanı rialı"),
    SDR("1 SDR (BVF-nin xüsusi borcalma hüquqları)"),
    TRY("1 Türkiyə lirəsi"),
    TWD("1 Tayvan dolları"),
    TJS("1 Tacikistan somonisi"),
    TMT("1 Türkmənistan manatı"),
    UAH("1 Ukrayna qrivnası"),
    JPY("100 Yaponiya yeni"),
    NZD("1 Yeni Zelandiya dolları"),
    XPD("Palladium"),
    XPT("Platin"),
    XAG("Gümüş"),
    XAU("Qızıl");

    private final String description;

    CurrencyEnum(String description) {
        this.description = description;
    }
}