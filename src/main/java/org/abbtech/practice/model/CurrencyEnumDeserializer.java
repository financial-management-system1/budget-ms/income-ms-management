package org.abbtech.practice.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.abbtech.practice.exception.MyEnumValidationException;

import java.io.IOException;
import java.util.Arrays;

// for defining correct enum
public class CurrencyEnumDeserializer extends JsonDeserializer<CurrencyEnum> {
    @Override
    public CurrencyEnum deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String value = p.getText().toUpperCase();
        return Arrays.stream(CurrencyEnum.values())
                .filter(e -> e.name().equals(value))
                .findFirst()
                .orElseThrow(() -> new MyEnumValidationException("Invalid currency name: " + value));
    }
}