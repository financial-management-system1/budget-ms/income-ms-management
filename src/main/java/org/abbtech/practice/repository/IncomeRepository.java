package org.abbtech.practice.repository;

import org.abbtech.practice.model.Income;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface IncomeRepository extends JpaRepository<Income, Long> {
    Optional<Income> findIncomeById(Long id);

    @Query("SELECT i FROM Income i WHERE i.userId = :userId AND MONTH(i.lastModifiedDate) = :month")
    Optional<List<Income>> findIncomesByUserIdAndMonth(@Param("userId") UUID userId, @Param("month") int month);

    @Query("SELECT i FROM Income i WHERE i.userId = :userId AND i.categoryIncome.categoryName = :categoryName")
    Optional<List<Income>> findIncomesByUserIdAndCategory(@Param("userId") UUID userId, @Param("categoryName") String categoryName);

    @Query("SELECT i FROM Income i WHERE i.userId = :userId AND i.categoryIncome.categoryName = :categoryName AND MONTH(i.lastModifiedDate) = :month")
    Optional<List<Income>> findIncomesByUserIdAndMonthAndCategory(@Param("userId") UUID userId, @Param("month") int month, @Param("categoryName") String categoryName);

    Optional<List<Income>> findIncomesByUserId(UUID userId);
}