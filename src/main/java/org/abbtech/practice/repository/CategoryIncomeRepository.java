package org.abbtech.practice.repository;

import org.abbtech.practice.model.CategoryIncome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryIncomeRepository extends JpaRepository<CategoryIncome, Long> {
    CategoryIncome findCategoryIncomeByCategoryName(String categoryName);
}