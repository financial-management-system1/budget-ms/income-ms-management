package org.abbtech.practice.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Setter
@Getter
public class BalanceRequest {
    private BigDecimal balance;
}