package org.abbtech.practice.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public record ExchangeRateDTO(String currencyCode, @JsonIgnore Double exchangeValue, Double convertedAmount) {
}
