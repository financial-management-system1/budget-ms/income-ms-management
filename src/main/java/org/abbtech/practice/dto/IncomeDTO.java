package org.abbtech.practice.dto;

import jakarta.validation.constraints.*;
import org.abbtech.practice.model.CurrencyEnum;

import java.time.LocalDateTime;

public record IncomeDTO(
        @Size(max = 100, message = "The length of the title cannot be more than 100 characters.")
        @NotBlank
        String title,
        @Size(max = 500, message = "The length of the notes cannot be more than 500 characters.")
        String notes,
        @NotNull
        @Min(value = 0, message = "The income cannot be negative value.")
        Double amount,
        LocalDateTime lastModifiedDate,
        @NotNull
        CurrencyEnum currencyName,
        @NotNull
        String categoryName
        ) {
}