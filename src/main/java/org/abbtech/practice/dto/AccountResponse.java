package org.abbtech.practice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record AccountResponse(BigDecimal limit, BigDecimal balance) {
}