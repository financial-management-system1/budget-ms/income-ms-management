package org.abbtech.practice.dto;

public record ExchangeRateRequest(String sourceCurrency, Double amount, String targetCurrency) {
}