package org.abbtech.practice.exception;

public class MyEnumValidationException extends RuntimeException {
    public MyEnumValidationException(String message) {
        super(message);
    }
}