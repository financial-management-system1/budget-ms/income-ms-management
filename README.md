# Income Management System

## Overview
The Income Management System is a backend service designed to help users track their incomes, create budgets, and manage their finances. It is built using a microservices architecture with the following components:
- **API Gateway**: Manages service routes and authentication.
- **User Service**: Handles user registrations, logins, and profiles.
- **Expense Service**: Manages expense tracking and categorization.
- **Budget Service**: Handles budget creation, modification, and tracking.
- **Notification Service**: Handles email notifications.

## Components
- **IncomeController**: REST controller for managing income-related operations.
- **DTOs**: Data Transfer Objects for handling data.
- **Exception Handling**: Global exception handler for managing application exceptions.
- **Model**: JPA entities for database operations.
- **Repository**: Spring Data JPA repositories for data access.
- **Service**: Business logic for managing incomes.
- **Service Clients**: Feign clients for interacting with external services.

## Features
- Add, edit, delete, and view incomes.
- Categorize incomes.
- Convert income amounts to AZN using an external exchange service.
- Update user balances based on total income.
- Fetch incomes by month and category.
- Calculate total and monthly income in AZN.

## Getting Started


## Prerequisites

- Java 17
- Gradle 7.x
- Docker (for running the microservices)

## Setup

1. **Clone the Repository**
    ```bash
    git clone <repository_url>
    cd <repository_name>
    ```

2. **Build the Project**
    ```bash
    ./gradlew build
    ```

3. **Run the Application**
    ```bash
    ./gradlew bootRun
    ```

## Running Tests

To run the unit tests, execute the following command:
    ```bash
    ./gradlew test
    ```

## API Endpoints
### IncomeController
#### Add Income

```bash
POST /add
Headers: x-userId: {userId}
Body: { "title": "Salary", "notes": "Monthly salary", "amount": 5000.0, "lastModifiedDate": "2023-07-11T00:00:00", "currencyName": "USD", "categoryName": "Salary" }
```

#### Edit Income

```bash
PUT /edit
Headers: x-userId: {userId}
Params: incomeId={incomeId}
Body: { "title": "Part-time", "notes": "Freelance work", "amount": 3000.0, "lastModifiedDate": "2023-07-11T00:00:00", "currencyName": "EUR", "categoryName": "Part-time" }
```

#### Delete Income

```bash
DELETE /delete
Headers: x-userId: {userId}
Params: incomeId={incomeId}
```

#### Get All Incomes

```bash
GET /get
Headers: x-userId: {userId}
```

#### Get Incomes of Month

```bash
GET /getByMonth
Headers: x-userId: {userId}
Params: month={1-12}
```

#### Get Incomes by Category

```bash
GET /getByCategory
Headers: x-userId: {userId}
Params: categoryName={categoryName}
```

#### Get Incomes by Month and Category

```bash
GET /getByMonthAndCategory
Headers: x-userId: {userId}
Params: month={1-12}, categoryName={categoryName}
```

#### Get Total Income

```bash
GET /getTotalIncome
Headers: x-userId: {userId}
```

#### Get Monthly Total Income

```bash
GET /getMonthlyTotalIncome
Headers: x-userId: {userId}
Params: month={1-12}
```

### Exception Handling
Global exception handling is managed by the GlobalExceptionHandler class which handles various exceptions such as validation errors, resource not found, and general exceptions.